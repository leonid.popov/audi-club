/* ============
 * Auth Module
 * ============
 */
/* eslint-disable */

import state from './state';
import contentBlocksAPI from '../../../api/contentBlocks';
import voucherAPI from '../../../api/voucher';

export default {
  // namespaced: true,
  state,
  actions: {
    changeAdminStyle ({ commit }, brand) {
      commit('changeAdminStyle', brand)
    },
    incrementAsync ({ commit }) {
      setTimeout(() => {
        commit('increment')
      }, 1000)
    },
    setOwnersOffers ({ commit }) {
      contentBlocksAPI.getContentBlocks('owners-offers').then((response) => {
        commit('setOwnersOffers', response);
      });
    },
    setCosmeticRepairs ({ commit }) {
      contentBlocksAPI.getContentBlocks('cosmetic-repairs').then((response) => {
        commit('setCosmeticRepairs', response);
      });
    },
    setVIPs ({ commit }) {
      contentBlocksAPI.getContentBlocks('little-vips').then((response) => {
        commit('setVIPs', response);
      });
    },
    setVouchers ({ commit }) {
      voucherAPI.getVouchers().then((response) => {
        commit('setVouchers', response.data);
      });
    },
    setLoading ({ commit }, loading) {
      commit('setLoading', loading);
    },
  },
  getters: {
    count3: state => {
      return state.count + 99
    },
    // Get color for the brand
    getAdminBrandStyles: (state) => (brand) => {
      return state.styles.adminPortal
      // return state.todos.find(todo => todo.id === id)
    },
    getLoading: (state) => {
      return state.loading
    },
    getBrands:(state) => () =>{
      return state.brands
    },
    getBrand:(state) => () =>{
      return state.brand
    },
    getMenus: (state) => () => {
      return state.menus
    },
    getHeader: (state) => {
      return state.header
    },
    getHome: (state) => () => {
      return state.home
    },
    getFooter: (state) => {
      return state.footer
    },
    getOffers: (state) => {
      return state.offers
    },
    getVIPs: (state) => {
      return state.vips
    },
    getVouchers: (state) => {
      return state.vouchers
    },
    getCosmeticRepairs: (state) => {
      return state.cosmeticrepairs
    },
    getLocations: (state) => () => {
      return state.locations
    },
    getMapInfo: (state) => () => {
      return state.mapInfo
    },
    getNews: (state) => () => {
      return state.news
    },
  },
  mutations: {
    changeAdminStyle(state, brand) {
      state.styles.adminPortal.backgroundColor = 'blue';
      switch (brand.brand) {
        case 'Honda':
          state.styles.adminPortal.backgroundColor = '#17a2b8';
          break;
        case 'Skoda':
          state.styles.adminPortal.backgroundColor = 'grey';
          break;
        case 'Kawasaki':
          state.styles.adminPortal.backgroundColor = '#46B87D';
          break;
        case 'Kawasaki':
          state.styles.adminPortal.backgroundColor = '#2252B8';
          break;
        default:
          state.styles.adminPortal.backgroundColor = 'grey';
      }
    },
    increment(state) {
      state.count++;
    },
    setOwnersOffers(state, offers) {
      state.offers = offers;
    },
    setCosmeticRepairs(state, cosmeticrepairs) {
      state.cosmeticrepairs = cosmeticrepairs;
    },
    setVIPs(state, vips) {
      state.vips = vips;
    },
    setVouchers(state, vouchers) {
      state.vouchers = vouchers;
    },
    setLoading(state, loading) {
      state.loading = loading;
    },
  },
  // // Using actions:
  // // dispatch with a payload
  //   store.dispatch('incrementAsync', {
  //     amount: 10
  //   })
  //
  //   // dispatch with an object
  //   store.dispatch({
  //     type: 'incrementAsync',
  //     amount: 10
  //   })
};
