/* ============
 * State of the auth module
 * ============
 *
 * The initial state of the auth module.
 */
/* eslint-disable */

export default {
  modelName: 'ssoosm',
  defaultPath: 'http://s3-eu-west-2.amazonaws.com/insitu-aws-s3-bucket-user-content/pRzvPthvp4u2sY5pe/',
  mtlPath: 'Valencia_Deep_Seating.mtl',
  objPath: 'Valencia_Deep_Seating.obj',
  matJsonFile: '/static/resources/materials/mat-lib-auto.json',
  count: 0,
  styles: {
    adminPortal: {
      backgroundColor: 'grey'
    }
  },
  brands:["Honda", "Skoda", "Kawasaki", "Seat"],
  loading: false,
  brand: "Audi",
  header: {
    title: {
      title1: 'Audi',
      title2: 'Club',
    },
    style: {
      banner_style: 'background-color: #000000;',
      menu_style: '',
    },
    welcome: {
      mob_hide: 'Hi Karen, welcome to our Sytner Audi Club.',
      desktop_hide: 'Welcome to Sytner Audi Club.',
    },
    news: [
      {
        id: 1,
        title: 'An announcement that can be edited in the database.',
      },
      {
        id: 2,
        title: 'Another announcement that can be edited in the database.',
      }
    ],
    menus: [
      { id: 1, name: 'Vouchers', link: '', children: [], routerName: 'home.vehicle.redeemvoucher' },
      { id: 2, name: 'Earn Rewards', link: '', children: [], routerName: '' },
      { id: 3, name: 'Owners Offers', link: '', children: [], routerName: 'home.vehicle.ownersoffers' },
      { id: 4, name: 'Cosmetic Repairs', link: '', children: [], routerName: 'home.vehicle.cosmeticrepairs' },
      { id: 5, name: 'Accident Assistance', link: '', children: [], routerName: 'home.vehicle.accedentcare' },
      { id: 6, name: 'Little VIPs', link: '', children: [], routerName: 'home.vehicle.littlevips' },
      { id: 7, name: 'News', link: '', children: [], routerName: '' },
      { id: 8, name: 'Locations', link: '', children: [], routerName: 'home.vehicle.locations' },
      { id: 9, name: 'Audi Connect', link: '', children: [], routerName: '' },
    ],
  },
  home: {
    slider: [
      {
        id: 1, image: 'video1.jpg', title: 'Exclusively yours', sub_title: 'ŠKODA Club - your access to savings, offers and events', alt: 'slider 1',
      },
      {
        id: 2, image: 'video2.jpg', title: 'Claim your free wash and vac', sub_title: 'Keep the shine on your pride and joy', alt: 'slider 2',
      },
      {
        id: 3, image: 'video3.jpg', title: 'View your user profile', sub_title: 'Take a moment to update your details', alt: 'slider 3',
      },
      {
        id: 4, image: 'video4.jpg', title: 'Claim your free top ups', sub_title: 'Let us check and top up your fluid levels', alt: 'slider 4',
      },
      {
        id: 5, image: 'video5.jpg', title: 'Earn rewards', sub_title: 'Recommend a friend and benefit if they purchase', alt: 'slider 5',
      },
      {
        id: 6, image: 'video6.jpg', title: 'Complimentary health check', sub_title: 'Let us check everything is working as it should', alt: 'slider 6',
      },
    ],
    section1: {
      row1: [
        {
          id: 1,
          section_class: 'dark',
          title: 'Five-Star Service',
          description: 'Recognise one of our team for making your experience special.',
          image1: 'making_it_special_icon.svg',
          image2: 'making_it_special.svg',
          href_style: 'background-color: #33414e;',
          img_style: 'top: 10%;',
          image_class: 'logo',
          href: '/vehicle/makingitspecial',
        },
        {
          id: 2,
          section_class: 'transparent',
          title: 'Redeem Vouchers',
          description: 'Enjoy complimentary benefits as a ŠKODA Club Member.',
          image1: 'download_icon.svg',
          image2: '',
          href_style: 'background-color: #bb0a30;',
          href: '/vehicle/redeemvoucher',
        },
        {
          id: 3,
          section_class: 'dark',
          title: 'Head of Business',
          description: 'Nathan Smith welcomes you to ŠKODA North Wales.',
          image1: 'feedback_icon.svg',
          image2: 'matthew_mills.jpg',
          href_style: 'background-color: #33414e;',
          h2_style: 'text-shadow: 0px 0px 5px #000;',
          img_style: 'object-position: 50% 50%; -o-object-position: 50% 50%;',
        },
      ],
      row2: [
        {
          id: 1,
          section_class: '',
          title: 'Owners Offers',
          description: 'Gain access to offers available exclusively to members.',
          image1: 'key_icon.svg',
          image2: 'owners.jpg',
          h2_style: '',
          img_style: 'object-position: 100% 100%; -o-object-position: 100% 100%;',
          href: '/vehicle/ownersoffers',
        },
        {
          id: 2,
          title: 'Earn Rewards',
          description: 'Recommend us to your friends and family to earn discount vouchers off servicing and purchases.',
          image1: 'rewards_icon.svg',
          image2: 'rewards.jpg',
          img_style: 'object-position: 30% 100%; -o-object-position: 30% 100%;',
          h2_style: 'text-shadow: 0px 0px 5px #fff;',
        },
      ],
    },
    section2: {
      row1: [
        {
          id: 1,
          section_class: 'dark',
          title: 'Cosmetic Repairs',
          description: 'Get damage to wheels, bumpers or bodywork fixed for less than you think.',
          image1: 'repairs_icon.svg',
          image2: 'scratch.jpg',
          h2_style: 'text-shadow: 0px 0px 5px #000;',
          href: '/vehicle/cosmeticrepairs',
        },
        {
          id: 2,
          section_class: 'dark',
          title: 'Accident Assistance',
          description: 'Make Sytnerdrive your first call if you\'ve had an accident.',
          image1: 'call_icon.svg',
          image2: 'accident.jpg',
          h2_style: 'text-shadow: 0px 0px 5px #000;',
          href: '/vehicle/accidentcare',
        },
      ],
      row2: [
        {
          id: 1,
          title: 'Little VIPs',
          description: 'Discover ŠKODA treats for toddlers and children.',
          image1: 'kids_icon.svg',
          image2: 'little-girl-with-teddy.jpg',
          img_style: 'object-position: 0% 50%; -o-object-position: 0% 50%;',
          h2_style: 'text-shadow: 0px 0px 5px #fff;',
          href: '/vehicle/littlevips',
        },
        {
          id: 2,
          title: 'Locations',
          description: 'Find a Sytner Audi centre.',
          image1: 'locations_icon.svg',
          image2: 'locate-a-centre.jpg',
          img_style: 'object-position: 50% 50%; -o-object-position: 50% 50%;',
          href: '/vehicle/locations',
        },
        {
          id: 3,
          a_class: 'dark',
          section_class: 'transparent',
          title: 'My Account',
          description: 'Edit your details.',
          image1: 'account_icon.svg',
          image2: '',
          href_style: 'background-color: #bb0a30;',
          href: '/account',
        },
      ],
    },
    section3: {
      row1: [
        {
          id: 1,
          a_class: '',
          section_class: '',
          title: 'Latest News',
          description: 'Discover the latest Audi news, including new model releases and technology updates.',
          image1: 'news_icon.svg',
          image2: 'latest_models.jpg',
          img_style: 'object-position: 50% 50%; -o-object-position: 50% 50%;',
        },
        {
          id: 2,
          a_class: 'dark',
          section_class: 'dark',
          title: 'Audi Connect',
          description: 'Download the app to synchronise your smart phone with your Audi.',
          image1: 'connect_icon.svg',
          image2: 'audi_connect.jpg',
          h2_style: 'text-shadow: 0px 0px 5px #000;',
          img_style: 'object-position: 100% 100%; -o-object-position: 100% 100%;',
        },
      ],
    },
  },
  cosmeticrepairs: [],
  offers: [
    // {
    //   id: 1,
    //   title: 'Car Finance made easy',
    //   title_style: 'color: black;',
    //   image: '/static/images/offer/offer1.png',
    // },
    // {
    //   id: 2,
    //   title: 'Tyres',
    //   title_style: 'color: white;',
    //   image: '/static/images/offer/offer2.png',
    // },
    // {
    //   id: 3,
    //   title: 'Parts',
    //   title_style: 'color: black;',
    //   image: '/static/images/offer/offer3.png',
    // },
    // {
    //   id: 4,
    //   title: 'ŠKODA Connect',
    //   title_style: 'color: black;',
    //   image: '/static/images/offer/offer15.png',
    //   rowspan: 2,
    // },
    // {
    //   id: 5,
    //   title: 'Trade Parts Specialists',
    //   title_style: 'color: white;',
    //   image: '/static/images/offer/offer5.png',
    // },
    // {
    //   id: 6,
    //   title: 'Free ŠKODA Health check',
    //   title_style: 'color: white;',
    //   image: '/static/images/offer/offer6.png',
    // },
    // {
    //   id: 7,
    //   title: 'Sytner Drive',
    //   title_style: 'color: black;',
    //   image: '/static/images/offer/offer7.png',
    // },
    // {
    //   id: 8,
    //   title: 'ŠKODA Warranty',
    //   title_style: 'color: white;',
    //   image: '/static/images/offer/offer8.png',
    // },
    // {
    //   id: 9,
    //   title: 'Tyres',
    //   title_style: 'color: white;',
    //   image: '/static/images/offer/offer14.png',
    //   colspan: 2,
    // },
    // {
    //   id: 10,
    //   title: 'Service Plans',
    //   title_style: 'color: white;',
    //   image: '/static/images/offer/offer10.png',
    // },
    // {
    //   id: 11,
    //   title: 'Fixed Price Servicing',
    //   title_style: 'color: white;',
    //   image: '/static/images/offer/offer6.png',
    // },
    // {
    //   id: 12,
    //   title: 'Service Promise',
    //   title_style: 'color: white;',
    //   image: '/static/images/offer/offer12.png',
    // },
    // {
    //   id: 13,
    //   title: 'Free ŠKODA Health check',
    //   title_style: 'color: white;',
    //   image: '/static/images/offer/offer13.png',
    // },
    // {
    //   id: 14,
    //   title: 'Free ŠKODA Health check',
    //   title_style: 'color: white;',
    //   image: '/static/images/offer/offer15.png',
    //   rowspan: 2,
    // },
  ],
  vips: [
    // {
    //   id: 1,
    //   image: '/static/images/vip/vip.png',
    //   vip_title: 'Competitions',
    //   vip_sub_title: 'Sytner ŠKODA competitions',
    //   header_title: 'Header will go here',
    //   header_content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam elit velit, \
    //   facilisis a rhoncus nec, facilisis vel leo. Nulla ac lobortis justo, eu feugiat\
    //   nisl.',
    //   header_bottom: 'Enter Competitions',
    // },
    // {
    //   id: 2,
    //   image: '/static/images/vip/vip.png',
    //   vip_title: 'Kids Activity Book',
    //   vip_sub_title: 'Download activty books for kids',
    //   header_title: 'Header will go here',
    //   header_content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam elit velit, \
    //   facilisis a rhoncus nec, facilisis vel leo. Nulla ac lobortis justo, eu feugiat\
    //   nisl.',
    //   header_bottom: 'Download Activity Book',
    // },
    // {
    //   id: 3,
    //   image: '/static/images/vip/vip.png',
    //   vip_title: 'Ride-on Cars',
    //   vip_sub_title: 'Children’s ŠKODA accessories range',
    //   header_title: 'Header will go here',
    //   header_content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam elit velit, \
    //   facilisis a rhoncus nec, facilisis vel leo. Nulla ac lobortis justo, eu feugiat\
    //   nisl.',
    //   header_bottom: 'View ŠKODA Accessories',
    // },
  ],
  news: [
    {
      id: 1,
      image: '/static/images/vip/vip.png',
      title: 'Header will go here',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam elit velit, \
      facilisis a rhoncus nec, facilisis vel leo. Nulla ac lobortis justo, eu feugiat\
      nisl.',
    },
    {
      id: 2,
      image: '/static/images/vip/vip.png',
      title: 'Header will go here',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam elit velit, \
      facilisis a rhoncus nec, facilisis vel leo. Nulla ac lobortis justo, eu feugiat\
      nisl.',
    },
    {
      id: 3,
      image: '/static/images/vip/vip.png',
      title: 'Header will go here',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam elit velit, \
      facilisis a rhoncus nec, facilisis vel leo. Nulla ac lobortis justo, eu feugiat\
      nisl.',
    },
  ],
  locations: [
    {
      id: 1,
      name: 'Audi City London',
      image: '/static/images/locations/location1.png',
      address: {
        address1: '74-75 Piccadilly',
        address2: 'London Greater London',
        address3: 'W1J 8HU',
      },
      telephone: '0207 495 0000',
      sales: {
        telephone: '020 3441 0880',
        days: [
          {
            name: 'Monday',
            start: '09:00',
            end: '19:00',
          },
          {
            name: 'Tuesday',
            start: '09:00',
            end: '19:00',
          },
        ],
      },
      services: {
        telephone: '020 3441 0890',
        days: [
          {
            name: 'Monday',
            start: '09:00',
            end: '19:00',
          },
          {
            name: 'Tuesday',
            start: '09:00',
            end: '19:00',
          },
        ],
      },
    },
    {
      id: 2,
      name: 'Bradford Audi',
      image: '/static/images/locations/location1.png',
      address: {
        address1: '74-75 Piccadilly',
        address2: 'London Greater London',
        address3: 'W1J 8HU',
      },
      telephone: '0207 495 0000',
      sales: {
        telephone: '020 3441 0880',
        days: [
          {
            name: 'Monday',
            start: '09:00',
            end: '19:00',
          },
          {
            name: 'Tuesday',
            start: '09:00',
            end: '19:00',
          },
        ],
      },
      services: {
        telephone: '020 3441 0890',
        days: [
          {
            name: 'Monday',
            start: '09:00',
            end: '19:00',
          },
          {
            name: 'Tuesday',
            start: '09:00',
            end: '19:00',
          },
        ],
      },
    }
  ],
  vouchers: [
    // {
    //   id: 1,
    //   title: 'Free Wash & Vac',
    //   valid_date: '01/06/2018',
    //   image: '/static/images/voucher/voucher1.png',
    // },
    // {
    //   id: 2,
    //   title: 'Free Top Ups',
    //   valid_date: '01/06/2018',
    //   image: '/static/images/voucher/voucher2.png',
    // },
    // {
    //   id: 3,
    //   title: 'Summer Health Checks',
    //   valid_date: '01/06/2018',
    //   image: '/static/images/voucher/voucher3.png',
    // },
  ],
  mapInfo: {
    position: {
      lat: 51.508742,
      lng: -0.120850,
    },
    address1: '74-75 Piccadilly London Greater London W1J 8HU',
    address2: '0207 495 0000',
    name: 'Audi City London',
  },
  footer: {
    contact: {
      address1: 'Nottingham Audi',
      address2: '11 Abbeyfield Road, Lenton Industrial Estate, Nottingham NG7 2SZ',
      phone: '0115 988 2820',
    },
    legal: 'This website uses cookies to enhance your experience, by continuing to use this site you agree to our\
    <a href="">Cookie Policy</a> and\
    <a href="">Terms &amp; Conditions</a>. Please\
    <a href="">view our policy here</a>. Sytner Group Ltd. Registered in England No. 2883766. Registered Office: 2 Penman Way,\
    Grove Park, Leicester, LE19 1ST. VAT No. 610625086. We do not distribute or sell your email addresses.',
  },
  // defaultPath: 'http://s3-eu-west-2.amazonaws.com/cedar-nursery/bsgEtY5bCXHXq3teK/',
  // mtlPath: 'cedars-4-sips-horsley-garden-building-3800-x-3600.mtl',
  // objPath: 'cedars-4-sips-horsley-garden-building-3800-x-3600.obj',
};
