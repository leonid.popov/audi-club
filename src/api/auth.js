import Vue from 'vue';
import config from './config';

const login = (email, password) => fetch(`${config.LOGIN_URL}/${email}/${password}`)
  .then(response => response.text());
const getUserInfo = () => Vue.$http.get(config.GET_USER_INFO_URL);

export default {
  login,
  getUserInfo,
};
