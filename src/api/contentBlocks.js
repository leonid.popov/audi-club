import config from './config';

const getContentBlocks = content => fetch(config.GET_CONTENT_BLOCK_URL(content))
  .then(response => response.json());

export default {
  getContentBlocks,
};
