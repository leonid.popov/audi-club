const API_BASE_URL = 'http://35.177.206.206:7070';
const BRAND = 'Audi';
const GET_CONTENT_BLOCK_URL = cbkind => `${API_BASE_URL}/cBlock/${BRAND}/${cbkind}/list`;
const LOGIN_URL = `${API_BASE_URL}/users/login`;
const GET_USER_INFO_URL = `${API_BASE_URL}/users/profile/get`;
const GET_VOUCHERS_URL = `${API_BASE_URL}/vouchers/getList/${BRAND}`;

export default {
  GET_CONTENT_BLOCK_URL,
  LOGIN_URL,
  GET_USER_INFO_URL,
  GET_VOUCHERS_URL,
};
