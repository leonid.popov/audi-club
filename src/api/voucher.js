import Vue from 'vue';
import config from './config';

const getVouchers = () => Vue.$http.get(config.GET_VOUCHERS_URL);

export default {
  getVouchers,
};
